package br.ufsm.pdp.prova1;

import br.ufsm.pdp.prova1.annotation.NoCopy;
import lombok.SneakyThrows;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.*;

public class Clonner {
    @SneakyThrows
    public static <T> T realizarCopiaProfunda(T objeto) {
        T instance = (T) objeto.getClass().getDeclaredConstructor().newInstance();

        List<Field> fields = Arrays.stream(objeto.getClass().getDeclaredFields())
            .filter(f -> (
                f.getAnnotation(NoCopy.class) == null
            ))
            .toList();

        for (Field field : fields) {
            Class<?> clone = instance.getClass();

            Field f = clone.getDeclaredField(field.getName());
            f.setAccessible(true);

            Object value = field.get(objeto);

            if (Date.class.isAssignableFrom(f.getType())) {
                value = new Date(((Date) value).getTime());
            } else if (LocalDate.class.isAssignableFrom(f.getType())) {
                value = LocalDate.ofEpochDay(((LocalDate) value).toEpochDay());
            } else if (Collection.class.isAssignableFrom(f.getType())) {
                value = value.getClass().getDeclaredConstructor(Collection.class).newInstance(value);
            } else if (!f.getType().isPrimitive() && !String.class.isAssignableFrom(f.getType()) && !f.getType().isEnum()) {
                value = Clonner.realizarCopiaProfunda(value);
            }

            f.set(instance, value);
        }

        return instance;
    }
}
