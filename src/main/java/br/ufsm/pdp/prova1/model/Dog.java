package br.ufsm.pdp.prova1.model;

import br.ufsm.pdp.prova1.annotation.NoCopy;
import lombok.ToString;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@ToString
public class Dog {
    public String name;

    @NoCopy
    public String pedigree;

    public int age;

    public Race race;

    public boolean hasOwner;

    public LocalDate adoptionDate;

    public List<String> placesVisited;
}
