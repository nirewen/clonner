package br.ufsm.pdp.prova1.model;

import lombok.ToString;

@ToString
public class Race {
    public enum FurType {
        SHORT,
        LONG
    };

    public String name;
    public FurType furType;
}
