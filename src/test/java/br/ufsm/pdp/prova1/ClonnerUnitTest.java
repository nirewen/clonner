package br.ufsm.pdp.prova1;

import br.ufsm.pdp.prova1.model.Dog;
import br.ufsm.pdp.prova1.model.Race;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ClonnerUnitTest {
    Dog dog = new Dog();

    @BeforeEach
    public void setup() {
        dog.name = "Jorge";
        dog.pedigree = "Almeida";
        dog.age = 3;
        dog.hasOwner = true;
        dog.adoptionDate = LocalDate.now();
        dog.placesVisited = new ArrayList<>();

        dog.placesVisited.add("Italia");
        dog.placesVisited.add("França");
        dog.placesVisited.add("Brasil");

        dog.race = new Race();
        dog.race.name = "Bulldog";
        dog.race.furType = Race.FurType.SHORT;
    }

    @Test
    public void testCopy() {
        Dog clone = Clonner.realizarCopiaProfunda(dog);

        dog.name = "Grant";
        dog.pedigree = "Trinidad";
        dog.race.furType = Race.FurType.LONG;
        dog.placesVisited.add("Argentina");

        assertNotEquals(dog.name, clone.name);
        assertNotEquals(dog.pedigree, clone.pedigree);
        assertEquals(dog.age, clone.age);
        assertNull(clone.pedigree);
        assertEquals(dog.hasOwner, clone.hasOwner);
        assertEquals(dog.adoptionDate.toEpochDay(), clone.adoptionDate.toEpochDay());
        assertNotEquals(dog.placesVisited, clone.placesVisited);
        assertEquals(dog.race.name, clone.race.name);
        assertNotEquals(dog.race.furType, clone.race.furType);

        System.out.println(dog);
        System.out.println(clone);
    }
}
